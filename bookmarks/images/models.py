from django.conf import settings
from django.db import models
# from django.urls import reverse
from django.utils.text import slugify


class Image(models.Model):
    """Модель изображения"""
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='images_created',
                             verbose_name='Создатель иображения')
    users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                        blank=True,
                                        related_name='images_liked',
                                        verbose_name='Пользователь, которому понравилось изображение')
    title = models.CharField(max_length=200,
                             verbose_name='Название')
    slug = models.SlugField(max_length=200,
                            blank=True)
    url = models.URLField(verbose_name='Адрес оригинала')
    image = models.ImageField(upload_to='images/%Y/%m/%d/',
                              verbose_name='Изображение')
    description = models.TextField(blank=True)
    created = models.DateField(auto_now_add=True,
                               db_index=True,
                               verbose_name='Дата создания')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Image, self).save(*args, **kwargs)

    # def get_absolute_url(self):
    #     return reverse('images:detail', args=[self.id, self.slug])

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
